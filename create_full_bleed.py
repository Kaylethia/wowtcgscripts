import os
from PIL import Image

setname = "EchoesofThunder" # input folder name TODO this should be a parameter
setshortname = "eot_test" # output folder name TODO this should be a parameter

# get filenames from input directory
cardsinset = os.listdir(setname)
# count the number of files for output purposes
numcards = str(len(cardsinset))
# set the counter to zero
currentcard = 0

# check if output folder exists, create it if required
if not os.path.exists(setshortname):
    os.mkdir(setshortname)
    print("Destination folder does not exist, creating...")

for card in cardsinset:
    # set the background to the same black color used for frames
    image = Image.new(mode="RGB", size=(825, 1125), color=(0, 0, 0))
    # load the current card image
    with Image.open(setname + "\\" + card) as cardimage:
        cardimage.load()
    # paste the image into the golden base image with the specified offset
    image.paste(cardimage, (43, 43))
    # if the filename ends with " back" (case insensitive) signify the hero backside
    if " back" in card.casefold():
        # split the filename by spaces, use the first element (card number) and append 'b'
        cardback = card.split()[0] + "b"
        # recreate the filename
        cardbackpath = cardback + card[card.find(" "):]
        # increment the counter
        currentcard += 1
        print("Converting card: " + str(currentcard) + "/" + numcards)
        # save the file to a folder using the set acronym as the folder name
        image.save(setshortname + "\\" + cardbackpath)
    # otherwise it's a regular card and doesn't need special treatment
    else:
        # increment the counter
        currentcard += 1
        print("Converting card: " + str(currentcard) + "/" + numcards)
        image.save(setshortname + "\\" + card)
