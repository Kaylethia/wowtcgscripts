# WoW tcg scripts

## Description

This is a collection of tools I made for personal use with WoW TCG Reborn cards and sets, to set up print files for use with printin services that require cards with full bleed images.
These scripts may require editing before use on Linux or MacOS.

## How to use these scripts

### get-galleryimages.ps1

A quick powershell script that I used to get Siege of Orgrimmar card images from the Reborn webiste. 
Create a folder named Siege of Orgrimmar in the same folder as the script and run the script. Once the script finishes, the folder will have the images for every card in the raid.

### rename_cards.py

Needs a tab-delimited text file as a parameter.
Example: `python rename_cards.py 'Heroes of Azeroth.txt'`
I copied the text from the table on http://www.wowcards.info/edition/azeroth/en and saved it to a text file named `Heroes of Azeroth.txt`
The images used need to be in a folder the same way (Heroes of Azeroth).
The images in the folder will be renamed to a more suitable scheme for the rest of the tools.

Manual edits are required for card backsides and certain special characters.

### make_setlist.py

Creates a decklist with one copy of every card in a set from a folder of card images.
Does not create decklist sections and adds hero backsides as separate cards.
Manual editing of the output is required.

Example: `python make_setlist.py itm`
Outputs a text file named setname.txt

### create_full_bleed.py and create_full_bleed_raid.py

Creates full bleed images from regular card images.
Currently the script needs to be edited to set the input and output folders. This will be changed to script parameters.

Example: `python create_full_bleed.py`

Outputs full bleed images to a specific named folder.

### pdf_from_decklist.py

Creates a print-service-ready pdf from a text-form decklist.

Example: `python pdf_from_decklist.py MurlocRogue.txt`

Outputs a pdf named MurlocRogue.pdf
