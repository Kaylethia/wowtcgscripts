# this should be a parameter, but the functionality is currently only useful for one raid
# grabs the Reborn gallery page, goes through every image found and if there's 
# a data-image element, grab the name, replace certain symbols to create a standardized filename
# and save to a file in a specified folder
$siege = Invoke-WebRequest https://www.wowtcgreborn.com/siege-raid
foreach ($card in $siege.Images) {
    if ($card.'data-image') {
        $placeholder = Split-Path $card.'data-image' -Leaf
        $placeholder = $placeholder.Replace("+", " ")
        $placeholder = $placeholder.Replace("%2C", ",")
        Invoke-WebRequest $card.'data-image' -OutFile ('.\Siege of Orgrimmar\' + $placeholder)
    }

}