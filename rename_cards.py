import argparse
import os
from pathlib import Path
# takes a file with numerically ordered cards and renames image files to conform to standard
# boilerplate argument parser to increase readability
parser = argparse.ArgumentParser()
# expect a filename as an argument and put them into a separate variable
parser.add_argument('filename')
args = parser.parse_args()


def stripextras(cardpath):
    # removes superfluous strings from filenames
    # TODO grab a setlist from the internet to validate
    # ideally the format should be 1-3 digits without leading zeroes, a string, and '.png'
    cardpath = cardpath.replace(" updated", "")
    cardpath = cardpath.replace(" Updated", "")
    cardpath = cardpath.replace(" Final", "")
    cardpath = cardpath.replace(" final", "")
    # replace backside with back to standardize names
    cardpath = cardpath.replace(" backside", " back")
    # strip leading zeroes
    cardpath = cardpath.lstrip("0")

# this uses a tab-delimited text file copied directly from http://www.wowcards.info/edition/azeroth/en
# read the file into a variable
decklist = Path(args.filename).read_text(encoding="utf-8")
# split the list into lines
decklistlines = decklist.splitlines()
# assumes the input folder and input text file have the same name
fullpath = args.filename.split(".")[0] + "\\"
# get a list of files in the input folder
importcards = os.listdir(args.filename.split(".")[0])
# get the number of files
numcards = str(len(importcards))
# initialize the counter
okcards = 0
for card in decklistlines:
    # create a filename from the first and second elements of the list (number and card name)
    newcard = card.split("\t")[0] + " " + card.split("\t")[1] + ".png"
    # get the card name from the second element of the list
    placeholder = card.split("\t")[1]
    # create a filename to get the original image
    oldcard = placeholder + ".png"
    # only used for Reborn cards, others should not need this
    stripextras(oldcard)
    # if the original file exists, increment the counter and rename the file
    if os.path.exists(fullpath + oldcard):
        okcards += 1
        os.rename(fullpath + oldcard, fullpath + newcard)
    else:
        print(card + " could not be converted")
    if os.path.exists(fullpath + placeholder + " - Backside.png"):
        newcard = card.split("\t")[0] + " " + card.split("\t")[1] + " - Backside.png"
        os.rename(fullpath + placeholder + " - Backside.png", fullpath + newcard)
# informational output
print("Ready to convert: " + str(okcards) + "/" + str(numcards))
