import os
import argparse
from pathlib import Path

# assumes the set folder is named using the short name (itm for Into the Mists)
# this script does not create decklist sections and requires manual editing

# boilerplate argument parser to increase readability
parser = argparse.ArgumentParser()
# expect a set name as an argument and put them into a separate variable
parser.add_argument('setname')
args = parser.parse_args()
# set up an output file setname.txt
outputfile = open(args.setname + ".txt", "w")

# get all files from the setname folder
cardsinset = os.listdir(args.setname)
# iterate over the cards
for card in cardsinset:
    # replace double spaces with signle spaces
    card = card.replace("  ", " ")
    # ignore tokens, they can be added manually
    if "Token" not in card:
        print(card[card.find(" ")+1:card.rfind(".")])
        # add one copy of the card to the output file
        # TODO ignore hero backsides
        outputfile.writelines("1 " + card[card.find(" ")+1:card.rfind(".")] + " (" + args.setname + ")\n")
# close the file, writing it to disk
outputfile.close()