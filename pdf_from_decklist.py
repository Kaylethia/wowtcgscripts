"""Generates a pdf file compatible with a printing service from a WoW tcg decklist."""
import argparse
import os
from pathlib import Path
from fpdf import FPDF
# boilerplate argument parser to increase readability
parser = argparse.ArgumentParser()
# expect a filename as an argument and put them into a separate variable
parser.add_argument('filename')
args = parser.parse_args()
# define constants for card size, accounting for 3mm of bleed on each side
# 69x94 is standard size with 3mm bleed on each side
CARDWIDTH = 69
CARDHEIGHT = 94
# set a default card back file, currently assumes it is in the specified location
CARDBACK = "itm\\0 Card Back.png"
# set up the pdf variable
pdf = FPDF('P', 'mm', (CARDWIDTH,CARDHEIGHT))

# function to add pages to the pdf
def addpage():
    """function to add two pages to the output file"""
    # required, adds a blank page
    pdf.add_page()
    # create a path to the image file
    cardimage = cardset + "\\" + cardfile
    # informational output
    print(cardimage)
    # add the image to the last page
    pdf.image(cardimage, x = 0, y = 0, w = CARDWIDTH,  h = CARDHEIGHT)
    # if the current decklist section is //play-1, get hero backsides and add them
    if currentsection == "//play-1":
        # required, adds a blank page
        pdf.add_page()
        # get the card number from the card number of the front side
        heroback = int(cardfile.split()[0])
        # finds the file based on the front side number + 'b'
        herobackpath = "".join(filter(lambda y: y.startswith(str(heroback) + 'b'), cardsinset))
        # create a path to the image file
        cardimage = cardset + "\\" + herobackpath
        # informational output
        print(cardimage)
        # add the image to the last page
        pdf.image(cardimage, x = 0, y = 0, w = CARDWIDTH,  h = CARDHEIGHT)
    else:
        # required, adds a blank page
        pdf.add_page()
        # add the specified generic card back
        pdf.image(CARDBACK, x = 0, y = 0, w = CARDWIDTH,  h = CARDHEIGHT)

# load decklist from a file
decklist = Path(args.filename).read_text(encoding="utf-8")
# and split it into lines
decklistlines = decklist.splitlines()
# keep a state variable "currentsection" for example,
# filling it dynamically as the script iterates over lines in the file,
# we only care about whether we're in //play-1, that is the only source of double-sided cards

currentsection = None

for line in decklistlines:
    # ignore empty lines
    if line != "":
        # if the line starts with //, set currentsection variable
        if line.startswith("//"):
            # set the current section to the current line
            currentsection = line
        # otherwise, we use regular functionality
        else:
            # get card count TODO this should be 1 if not present
            cardcount = int(line.split()[0])
            # get card name
            cardname = line[line.find(" ")+1:line.rfind(" ")]
            # get set from the end of the line, ignoring parentheses
            cardset = line[line.rfind(" ")+2:-1]
            # get a list of files in the folder
            cardsinset = os.listdir(cardset)
            # match the card name to a file, outputting the filename
            cardfile = "".join(next(filter(lambda x: cardname.casefold() in x.casefold(), cardsinset)))
            # loop for a number of times
            for z in range(cardcount):
                # call the function
                addpage()

# output the file, this can take a long time
pdfname = args.filename.split(".")[0]
pdf.output(name = pdfname + ".pdf")
# informational output
print("DONE")
